// omd_20_fe_200n.h



#ifndef OPTO_FORCE_OMD_H
#define OPTO_FORCE_OMD_H


#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <omd/opto.h>
#include <omd/sensorconfig.h>
#include <omd/optopackage.h>
#include <boost/thread.hpp>



namespace opto_force {

typedef unsigned long long mytime_t;

class OptoforceDAQ
{
    std::string deviceName;
    std::string name;
    std::string serialNumber;
    int version;
    double forceScale;
protected:
    bool running;
    int iPortIndex;
    int iSpeed;
    int iFilter;
    double Fx, Fy, Fz;
    double Tx, Ty, Tz;
    double Fx0, Fy0, Fz0;

    OptoDAQ optoDaq;
    OptoPorts ports;

    boost::thread daqThread;
    boost::mutex mut;

public:
    OptoforceDAQ(int port, int speed, int filter);
    OptoforceDAQ();
    ~OptoforceDAQ();
    std::vector<double> GetForceN();
    double GetFxN();
    double GetFyN();
    double GetFzN();
    void SetForceScale(double scale);
    void zeroNow(bool z);
    void Start();
    void Stop();
    void MySleep(unsigned long p_uMillisecs);
    mytime_t Now();
    mytime_t NowMicro();
    mytime_t ElapsedTime(mytime_t p_Time);
    mytime_t ElapsedTimeMicro(mytime_t p_Time);
    bool DaqThreadFunction();
    bool OpenPort(OptoDAQ & p_optoDAQ, OptoPorts & p_Ports, int p_iIndex);
    bool SetConfig(OptoDAQ & p_optoDAQ, int p_iSpeed, int p_iFilter);
    void SaveInformation(OptoDAQ & p_optoDAQ, OPort & p_Port);
    bool Is3DSensor(OptoDAQ & p_optoDAQ);
    int ReadPackage3D(OptoDAQ & p_optoDAQ, OptoPackage & p_Package);
    int ReadPackage6D(OptoDAQ & p_optoDAQ, OptoPackage6D & p_Package);
    void Run3DSensor(OptoDAQ & p_optoDAQ);
    void Run6DSensor(OptoDAQ & p_optoDAQ);
};



}



#endif
