// omd_20_fe_200n_nodelet.h



#ifndef OPTO_FORCE_OMD_20_FE_200_N_NODELET_H
#define OPTO_FORCE_OMD_20_FE_200_N_NODELET_H



#include <nodelet/nodelet.h>
#include "omd_20_fe_200n.h"



namespace opto_force {



class OMD20FE200NNodelet : public OptoforceDAQ, public nodelet::Nodelet
{
public:
    OMD20FE200NNodelet();
    void onInit();
    ~OMD20FE200NNodelet();
};



}



#endif
