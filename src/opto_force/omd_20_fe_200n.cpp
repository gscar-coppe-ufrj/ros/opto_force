// omd_20_fe_200n.h



#include <opto_force/omd_20_fe_200n.h>



opto_force::OptoforceDAQ::OptoforceDAQ(int port = 0, int speed = 1000, int filter = 15):
    iPortIndex(port), iSpeed(speed), iFilter(filter)
{
    Fx = Fy = Fz = Tx = Ty = Tz = Fx0 = Fy0 = Fz0 = 0.;
    forceScale = 1.0;
}

opto_force::OptoforceDAQ::OptoforceDAQ()
{
    iPortIndex = 0;
    iSpeed = 1000;
    iFilter = 15;
    Fx = Fy = Fz = Tx = Ty = Tz = Fx0 = Fy0 = Fz0 = 0.;
    forceScale = 1.0;
}


opto_force::OptoforceDAQ::~OptoforceDAQ()
{
}

std::vector<double> opto_force::OptoforceDAQ::GetForceN()
{
    mut.lock();
    std::vector<double> v;
    v.push_back((Fx - Fx0) * forceScale);
    v.push_back((Fy - Fy0) * forceScale);
    v.push_back((Fz - Fz0) * forceScale);
    mut.unlock();
    return v;
}

double opto_force::OptoforceDAQ::GetFxN()
{
    boost::mutex::scoped_lock lock(mut);
    return ((Fx - Fx0) * forceScale);
}

double opto_force::OptoforceDAQ::GetFyN()
{
    boost::mutex::scoped_lock lock(mut);
    return ((Fy - Fy0) * forceScale);
}

double opto_force::OptoforceDAQ::GetFzN()
{
    boost::mutex::scoped_lock lock(mut);
    return ((Fz - Fz0) * forceScale);
}

void opto_force::OptoforceDAQ::SetForceScale(double scale)
{
    forceScale = scale;
}

void opto_force::OptoforceDAQ::zeroNow(bool reset)
{
    if (reset)
    {
        Fx0 = Fx;
        Fy0 = Fy;
        Fz0 = Fz;
    }
    else
    {
        Fx0 = 0;
        Fy0 = 0;
        Fz0 = 0;
    }
}

void opto_force::OptoforceDAQ::Start()
{
    this->daqThread = boost::thread(&opto_force::OptoforceDAQ::DaqThreadFunction, this);
    running = true;
}

void opto_force::OptoforceDAQ::Stop()
{
    boost::mutex::scoped_lock lock(mut);
    running = false;
    daqThread.join();
}

bool opto_force::OptoforceDAQ::DaqThreadFunction()
{
    if (OpenPort(optoDaq, ports, iPortIndex) == false)
    {
            std::cout<<"Could not open port"<<std::endl;
            return 0;
    }
    bool bConfig = SetConfig(optoDaq, iSpeed, iFilter);
    if (bConfig == false)
    {
            std::cout<<"Could not set config"<<std::endl;
            optoDaq.close();
            return 0;
    }
    if (Is3DSensor(optoDaq))
        Run3DSensor(optoDaq);
    else
        Run6DSensor(optoDaq);

    optoDaq.close();
}

bool opto_force::OptoforceDAQ::OpenPort(OptoDAQ &p_optoDAQ, OptoPorts & p_Ports, int p_iIndex)
{
    MySleep(2500); // We wait some ms to be sure about OptoPorts enumerated PortList
    OPort * portList = p_Ports.listPorts(true);
    int iLastSize = p_Ports.getLastSize();
    if (p_iIndex >= iLastSize) {
        // index overflow
        return false;
    }
    bool bSuccess = p_optoDAQ.open(portList[p_iIndex]);
    if (bSuccess)
        SaveInformation(p_optoDAQ, portList[p_iIndex]);
    return bSuccess;
}

bool opto_force::OptoforceDAQ::SetConfig(OptoDAQ &p_optoDAQ, int p_iSpeed, int p_iFilter)
{
    SensorConfig sensorConfig;
    sensorConfig.setSpeed(p_iSpeed);
    sensorConfig.setFilter(p_iFilter);
    mytime_t tNow = Now();

    bool bSuccess = false;
    do {
        bSuccess = p_optoDAQ.sendConfig(sensorConfig);
        if (bSuccess)
            return true;
        if (ElapsedTime(tNow) > 1000)
            return false;
        MySleep(1);
    } while (bSuccess == false);
    return false;
}

void opto_force::OptoforceDAQ::SaveInformation(OptoDAQ &p_optoDAQ, OPort &p_Port)
{
    deviceName = std::string(p_Port.deviceName);
    name = std::string(p_Port.name);
    serialNumber = std::string (p_Port.serialNumber);
    version = p_optoDAQ.getVersion();
}

void opto_force::OptoforceDAQ::MySleep(unsigned long p_uMillisecs)
{
    usleep(p_uMillisecs * 1000);
}

opto_force::mytime_t opto_force::OptoforceDAQ::Now()
{
    struct timespec t;
    clock_gettime(CLOCK_REALTIME, &t);
    mytime_t millisecs = t.tv_sec * 1000;
    millisecs += t.tv_nsec / (1000 * 1000);
    return millisecs;
}

opto_force::mytime_t opto_force::OptoforceDAQ::NowMicro()
{
    struct timespec t;
    clock_gettime(CLOCK_REALTIME, &t);
    mytime_t microsecs = t.tv_sec * 1000 * 1000;
    microsecs += t.tv_nsec / (1000);
    return microsecs;
}

opto_force::mytime_t opto_force::OptoforceDAQ::ElapsedTime(opto_force::mytime_t p_Time)
{
    return Now() - p_Time;
}

opto_force::mytime_t opto_force::OptoforceDAQ::ElapsedTimeMicro(opto_force::mytime_t p_Time)
{
    return NowMicro() - p_Time;
}

bool opto_force::OptoforceDAQ::Is3DSensor(OptoDAQ &p_optoDAQ)
{
    opto_version optoVersion = p_optoDAQ.getVersion();
    if (optoVersion != _95 && optoVersion != _64)
        return true;
    return false;
}

int opto_force::OptoforceDAQ::ReadPackage3D(OptoDAQ &p_optoDAQ, OptoPackage &p_Package)
{
    int iSize = -1;
    mytime_t tNow = Now();
    for (;;)
    {
        iSize = p_optoDAQ.read(p_Package, 0, false);
        if (iSize < 0 || iSize > 0)
            break;
        // No packages in the queue so we check the timeout
        if (ElapsedTime(tNow) >= 1000)
            break;
        MySleep(1);
    }
    return iSize;
}

int opto_force::OptoforceDAQ::ReadPackage6D(OptoDAQ &p_optoDAQ, OptoPackage6D &p_Package)
{
    int iSize = -1;
    mytime_t tNow = Now();
    for (;;)
    {
        iSize = p_optoDAQ.read6D(p_Package, false);
        if (iSize < 0 || iSize > 0)
            break;
        // No packages in the queue so we check the timeout
        if (ElapsedTime(tNow) >= 1000)
            break;
        MySleep(1);
    }
    return iSize;
}

void opto_force::OptoforceDAQ::Run3DSensor(OptoDAQ &p_optoDAQ)
{
    mytime_t tNow = Now();
    unsigned int uTotalReadPackages = 0;
    while (running)
    {
        mytime_t tLoopNow = NowMicro();
        OptoPackage optoPackage;
        int iReadSize = ReadPackage3D(p_optoDAQ, optoPackage);
        if (iReadSize < 0)
        {
            std::cout<<"Something went wrong, DAQ closed!"<<std::endl;
            return;
        }

        Fx = (double)(optoPackage.x);
        Fy = (double)(optoPackage.y);
        Fz = (double)(optoPackage.z);
        //std::cout<<"x: "<<optoPackage.x<<" y: "<<optoPackage.y<<" z: "<<optoPackage.z<<std::endl;
        uTotalReadPackages += (unsigned int)iReadSize;

        // Formatting output in C style
        double dLoopTime = ElapsedTimeMicro(tLoopNow) / 1000.0;
        mytime_t TotalElapsedTime = ElapsedTime(tNow);
        double dTotalTime = (double)TotalElapsedTime / 1000.0; // Elapsed time in sec
        double dFrequency = 0.0;
        if (dTotalTime > 0.0) {
            dFrequency = (uTotalReadPackages / dTotalTime);
        }
        //fprintf(stdout, "Elapsed: %.1f s Loop time: %.2f ms Samples: %u Sample rate: %.2f Hz\r\n", dTotalTime, dLoopTime, uTotalReadPackages, dFrequency);
        fflush(stdout);
    }
}

void opto_force::OptoforceDAQ::Run6DSensor(OptoDAQ &p_optoDAQ)
{
    mytime_t tNow = Now();
    unsigned int uTotalReadPackages = 0;
    while (running)
    {
        mytime_t tLoopNow = NowMicro();
        OptoPackage6D optoPackage;
        int iReadSize = ReadPackage6D(p_optoDAQ, optoPackage);
        if (iReadSize < 0) {
            std::cout<<"Something went wrong, DAQ closed!"<<std::endl;
            return;
        }
        uTotalReadPackages += (unsigned int)iReadSize;

        Fx = optoPackage.Fx;
        Fy = optoPackage.Fy;
        Fz = optoPackage.Fz;

        Tx = optoPackage.Tx;
        Ty = optoPackage.Ty;
        Tz = optoPackage.Tz;

        // Formatting output in C style
        double dLoopTime = ElapsedTimeMicro(tLoopNow) / 1000.0;
        mytime_t TotalElapsedTime = ElapsedTime(tNow);
        double dTotalTime = (double)TotalElapsedTime / 1000.0; // Elapsed time in sec
        double dFrequency = 0.0;
        if (dTotalTime > 0.0) {
            dFrequency = (uTotalReadPackages / dTotalTime);
        }
        fprintf(stdout, "Elapsed: %.1f s Loop time: %.2f ms Samples: %u Sample rate: %.2f Hz\r\n", dTotalTime, dLoopTime, uTotalReadPackages, dFrequency);
        fflush(stdout);
    }
}

